﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using uss.Models;
using uss.Models.ViewModels;

namespace uss.Controllers
{
    public class ReservesController : Controller
    {
        // GET: Reserves
        public ActionResult Index()
        {
            List<ListReservesViewModels> lst;
            using (sailorsEntities db = new sailorsEntities())
            {

                lst = (from d in db.reserves
                       select new ListReservesViewModels
                       {
                           sid = d.sid,
                            
                           bid = d.bid,
                          
                           day= d.day,
                       }).ToList();
            }
            return View(lst);
        }

        public ActionResult Nuevo()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Nuevo(ReservesViewModels model)
        {
            try
            {

                if (ModelState.IsValid)
                {
                    using (sailorsEntities db = new sailorsEntities())
                    {
                        var oTabla = new reserves();
                        oTabla.day = model.day;
                        oTabla.bid = model.bid;                  
                        oTabla.sid = model.sid;

                        db.reserves.Add(oTabla);
                        db.SaveChanges();
                    }
                    return Redirect("~/Reserves/index");
                }
                return View(model);

            }


            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public ActionResult Editar(int id)
        {
            ReservesViewModels model = new ReservesViewModels();
            using (sailorsEntities db = new sailorsEntities())
            {
                var oTabla = db.reserves.Find(id);
                model.sid = oTabla.sid;
              
                model.bid= oTabla.bid;
                
                model.day = oTabla.day;

            }
            return View(model);
        }
        [HttpPost]
        public ActionResult Editar(ReservesViewModels model)
        {
            try
            {

                if (ModelState.IsValid)
                {
                    using (sailorsEntities db = new sailorsEntities())
                    {
                        var oTabla = db.reserves.Find(model.sid);
                        oTabla.sid = model.sid;
                     
                       
                        oTabla.bid = model.bid;
                        oTabla.day = model.day;

                        db.Entry(oTabla).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                    return Redirect("~/Reserves/index");
                }
                return View(model);

            }


            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        [HttpGet]

        public ActionResult Eliminar(int id)
        {

            using (sailorsEntities db = new sailorsEntities())
            {
                var oTabla = db.reserves.Find(id);
                db.reserves.Remove(oTabla);
                db.SaveChanges();

            }
            return Redirect("~/Reserves/Index");
        }
    }
}