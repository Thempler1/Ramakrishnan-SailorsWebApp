﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using uss.Models;
using uss.Models.ViewModels;

namespace uss.Controllers
{
    public class SailorsController : Controller
    {
        // GET: Sailors
        public ActionResult Index()
        {
            List<ListSailorsViewModels> lst;
            using (sailorsEntities db= new sailorsEntities())
            {

                 lst = (from d in db.sailors
                           select new ListSailorsViewModels
                           {
                               sid = d.sid,
                               sname = d.sname,
                               rating = d.rating,
                               age = d.age
                           }).ToList();
            }
            return View(lst);
        }

        public ActionResult Nuevo()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Nuevo(SailorsViewModels model)
        {
            try
            {

                if (ModelState.IsValid)
                {
                    using (sailorsEntities db= new sailorsEntities())
                    {
                        var oTabla = new sailors();
                        oTabla.sname = model.sname;
                        oTabla.rating = model.rating;
                        oTabla.sid = model.sid;
                        oTabla.age = model.age;

                        db.sailors.Add(oTabla);
                        db.SaveChanges();
                    }
                    return Redirect("~/Sailors/index");
                }
                return View(model);
                
            }

                
                catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public ActionResult Editar(int id)
        {
            SailorsViewModels model = new SailorsViewModels();
            using (sailorsEntities db= new sailorsEntities())
            {
                var oTabla = db.sailors.Find(id);
                model.sname = oTabla.sname;
                model.age = oTabla.age;
                model.rating = oTabla.rating;
                model.sid = oTabla.sid;

            }
            return View(model);
        } 
        [HttpPost]
        public ActionResult Editar(SailorsViewModels model)
        {
            try
            {

                if (ModelState.IsValid)
                {
                    using (sailorsEntities db = new sailorsEntities())
                    {
                        var oTabla = db.sailors.Find(model.sid);
                        oTabla.sname = model.sname;
                        oTabla.rating = model.rating;
                        oTabla.sid = model.sid;
                        oTabla.age = model.age;

                        db.Entry(oTabla).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                    return Redirect("~/Sailors/index");
                }
                return View(model);

            }


            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        [HttpGet]

        public ActionResult Eliminar(int id)
        {
            
            using (sailorsEntities db = new sailorsEntities())
            {
                var oTabla = db.sailors.Find(id);
                db.sailors.Remove(oTabla);
                db.SaveChanges();

            }
            return Redirect("~/Sailors/Index");
        }

    }

}