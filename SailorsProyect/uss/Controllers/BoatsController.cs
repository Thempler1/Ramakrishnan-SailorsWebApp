﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using uss.Models;
using uss.Models.ViewModels;

namespace uss.Controllers
{
    public class BoatsController : Controller

    {

        public ActionResult Index()
        {
            List<ListBoatsViewModels> lst;
            using (sailorsEntities db = new sailorsEntities())
            {

                lst = (from d in db.boats
                       select new ListBoatsViewModels
                       {
                           bid = d.bid,
                           bname = d.bname,
                           color = d.color,
                          
                       }).ToList();
            }
            return View(lst);
        }

        public ActionResult Nuevo()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Nuevo(BoatsViewModels model)
        {
            try
            {

                if (ModelState.IsValid)
                {
                    using (sailorsEntities db = new sailorsEntities())
                    {
                        var oTabla = new boats();
                        oTabla.bname = model.bname;
                        oTabla.bid = model.bid;
                        oTabla.color = model.color;
                        

                        db.boats.Add(oTabla);
                        db.SaveChanges();
                    }
                    return Redirect("~/Boats/index");
                }
                return View(model);

            }


            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public ActionResult Editar(int id)
        {
            BoatsViewModels model = new BoatsViewModels();
            using (sailorsEntities db = new sailorsEntities())
            {
                var oTabla = db.boats.Find(id);
                model.bname = oTabla.bname;
                model.bid = oTabla.bid;
                model.color = oTabla.color;
              

            }
            return View(model);
        }
        [HttpPost]
        public ActionResult Editar(BoatsViewModels model)
        {
            try
            {

                if (ModelState.IsValid)
                {
                    using (sailorsEntities db = new sailorsEntities())
                    {
                        var oTabla = db.boats.Find(model.bid);
                        oTabla.bname = model.bname;
                        oTabla.bid = model.bid;
                        oTabla.color = model.color;
                       

                        db.Entry(oTabla).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                    return Redirect("~/Boats/index");
                }
                return View(model);

            }


            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        [HttpGet]

        public ActionResult Eliminar(int id)
        {

            using (sailorsEntities db = new sailorsEntities())
            {
                var oTabla = db.boats.Find(id);
                db.boats.Remove(oTabla);
                db.SaveChanges();

            }
            return Redirect("~/Boats/Index");
        }
    }
}