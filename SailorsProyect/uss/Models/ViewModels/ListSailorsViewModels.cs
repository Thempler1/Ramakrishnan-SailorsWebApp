﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace uss.Models.ViewModels
{
    public class ListSailorsViewModels
    {
        public int sid { get; set; } 
        public string sname { get; set; }
        public Nullable<int>  rating { get; set; }
        public Nullable<float> age { get; set; }

    }
}