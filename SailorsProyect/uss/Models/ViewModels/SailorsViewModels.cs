﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace uss.Models.ViewModels
{
    public class SailorsViewModels
    {
   
        public int sid { get; set; }
        [Required]
        [Display(Name ="Nombre")]
        public string sname { get; set; }
        [Required]
        [Display(Name = "Rating")]
        public Nullable<int> rating { get; set; }
        [Required]
        [Display(Name = "edad")]
        public Nullable<float> age { get; set; }
    }

   

    
}