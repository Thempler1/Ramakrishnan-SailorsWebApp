﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace uss.Models.ViewModels
{
    public class BoatsViewModels
    {

        public int bid { get; set; }
        [Required]
        [Display(Name = "Nombre del bote")]
        public string bname { get; set; }
        [Required]
        [Display(Name = "Color")]
        public string color { get; set; }
    }
}