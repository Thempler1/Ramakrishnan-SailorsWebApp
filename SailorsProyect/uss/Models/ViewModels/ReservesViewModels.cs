﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace uss.Models.ViewModels
{
    public class ReservesViewModels
    {
            public int sid { get; set; }
            public int bid { get; set; }
            [Required]
            [DataType(DataType.Date)]
            [Display(Name = "Fecha de Reserva")]
            [DisplayFormat(DataFormatString = "{yyyy-MM-dd}", ApplyFormatInEditMode = true)]
            public System.DateTime day { get; set; }

         
        
    }
}