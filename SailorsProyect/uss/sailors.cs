//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace uss
{
    using System;
    using System.Collections.Generic;
    
    public partial class sailors
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public sailors()
        {
            this.reserves = new HashSet<reserves>();
        }
    
        public int sid { get; set; }
        public string sname { get; set; }
        public Nullable<int> rating { get; set; }
        public Nullable<float> age { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<reserves> reserves { get; set; }
    }
}
