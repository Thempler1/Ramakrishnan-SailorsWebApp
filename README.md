# Ramakrishnan-SailorsWebApp

## Descripción 🚀

Ramakrishnan-SailorsWebApp es un Web-App que busca realizar acciones CRUD entre
las entidades "Sailor", "Boat" y "Reserves", permitiendo almacenar los cambios 
en una base de datos.

## Construcción y Herramientas 🛠️
SO Utilizado:
* Windows 10

Softwares Utilizados:
* Visual Studio 2019 (IDE), Proyecto MVC .Net utilizando C# y Entity Framework
* SQL SERVER EXPRESS 2017
* Microsoft SQL Server Management Studio 18

## Pre-requisitos 📋
- Instalar los softwares utilizados.
- Clonar o descargar este repositorio.

### Preparar la Base de Datos
Antes de poder hacer la puesta en marcha de la Web-App se debe configurar la base
de datos SQL SERVER de Microsoft SQL Server Management Studio 18, habilitando el
usuario "sa" y su contraseña como "123".

Una vez conectado con las credenciales de "sa" correr el script contenido en 
"sailors-sqlserver.txt" (copiar y pegar dentro de una nueva query).

### Preparar el Proyecto
Una vez lista la base de datos, abrir el archivo "uss.sln" con Visual Studio IDE,
dentro del proyecto ubicaremos el archivo "Web.config", donde tendremos que buscar
la etiqueta "<connectionStrings>" la cual contiene los datos de conexión a la base
de datos, una vez allí editar el atributo "source=", el que debe ser igual al nombre
de nuestro servidor (Si no recuerdas el nombre del servidor puedes verlo dentro de
Microsoft SQL Server Management Studio 18).

## Ejecución 💻
Una vez configurado nuestro entorno, podemos montar nuestra Web-App utilizando 
Visual Studio IDE, el cual muestra nuestro aplicativo en https://localhost:44306/ 

### Notas adicionales
Para mejorar la experienca de usuario se ha creado UX-Index, el cual nos otorga
otra vista de HOME, que utiliza CSS,JS,SAAS y otras librerias. Esta HOME es más 
amigable con el usuario y su uso redirige a las otras vistas generadas en el proyecto.

## Autores ✒️
* **Jair Leyton** - [Thempler1](https://gitlab.com/Thempler1/)
* **Jose Jimenez** 
* **Ramiro Larenas** 

## Información Adicional 📄
Este proyecto ha sido realizado por estudiantes de Ingeniería Civil en Informática de la Universidad San Sebastián
en la asignatura de Base de Datos.
* Profesor Felipe Manriquez.

